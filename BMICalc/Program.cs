﻿using System;

namespace BMICalc
{
    class Program
    {
        static void Main(string[] args)
		{   //variabelen declareren
			double height;
			double weight;
			double bmi;

			//invoer voor gebruiker
			Console.WriteLine("Uw lengte in meters (voorbeeld: 1,90): ");
			height = double.Parse(Console.ReadLine());

			Console.WriteLine("Uw gewicht in kilogram (voorbeeld: 83,0): ");
			weight = double.Parse(Console.ReadLine());


			bmi = weight / (height * height);

			Console.WriteLine("Uw BMI = " + bmi);

			//if statements om te bepalen of je een gezond gewicht hebt
			if (bmi >= 18 && bmi <= 24)
			{

				Console.WriteLine("Uw BMI is " + bmi + " U heeft een normaal gewicht.");
				Console.ReadKey();

			}

			if (bmi < 18)

			{


				Console.WriteLine("Uw BMI is " + bmi + " U heeft ondergewicht.");
				Console.ReadKey();

			}

			if (bmi > 24)

			{


				Console.WriteLine("Uw BMI is " + bmi + " U heeft overgewicht.");
				Console.ReadKey();

			}

		}
	}
    
}
